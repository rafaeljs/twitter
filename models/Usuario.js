const mongoose = require('mongoose');

var schemaUsuario = new mongoose.Schema({


    username: {
        type: String,
        required: true,
        unique: true
    },
    pass: {
        type: String,
        required: true
    },
    follow: [{  //usuario armazena um vetor de Id dos usuarios q ele segue
        type: String,
        ref: "usuario"
    }]
}, {collection : 'usuarios'});

var Usuario = mongoose.model('usuario',schemaUsuario);
module.exports = Usuario;