const mongoose = require('mongoose');
const tweet = require('./Tweet');

var schemaTweet = new mongoose.Schema({
    postedById: {
        type: String,
        ref: 'usuario'
    },
    postedBy:{
        type: String,
        ref: 'usuario'
    },
    tweetId:{
        type: String,
        ref: 'tweet'
    },
    time : {
        type : Date,
        default: Date.now()
    },
    message : String
}, {collection : 'comentarios'});

var Comentario = mongoose.model('comentario',schemaTweet);
module.exports = Comentario;