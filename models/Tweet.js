const mongoose = require('mongoose');
const usuario = require('./Usuario');
const comentario = require('./Comentario');


var schemaTweet = new mongoose.Schema({
    postedById: {
        type: String,
        ref: 'usuario'
    },
    postedBy:{
        type: String,
        ref: 'usuario'
    },
    time : {
        type : Date,
        default: Date.now()
    },
    message : String,
    comments : [{ //tweet armazenam um vetor de Id de comentarios
        type: String,
        ref: 'comentario'
                }]
} ,{collection : 'tweet'});

var Tweet = mongoose.model('tweet',schemaTweet);
module.exports = Tweet;