var express = require('express');
var router = express.Router();
var usuario = require('../models/Usuario');
var tweet = require('../models/Tweet');
var comentario = require('../models/Comentario');
const mongoose = require('mongoose');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/login', function (req,res, next) {
    var novoUsuario = usuario;
    novoUsuario.findOne({username: req.body.username, pass : req.body.pass}, function (err, user) {
        if(err){
            console.log(err);
            res.status(500).send();
        }
        if(!user){
            res.status(404).send();
        }
        req.session.authenticated = true;
        req.session.username = req.body.username;
        return res.status(200).send();
    })
});

router.post('/registro', function (req,res) {

   var novoUsuario = new usuario();
   novoUsuario.username = req.body.username;
   novoUsuario.pass = req.body.pass;
   novoUsuario.save(function (err, usuarioSalvo) {
       if(err){
           console.log(err.message);
         if(err.code === 11000){
             res.status(409).send({"mensagem":"Username duplicado!!"});
         }
         else if (err.message.includes("Path `username` is required")){
             res.status(412).send({"mensagem":"Username necessario!!"});
         }
         else if(err.message.includes("Path `pass` is required")){
             res.status(412).send({"mensagem":"Senha necessario!!"});
         }
         res.status(500).send();
       }
       res.status(200).send();
   })
});

router.post('/:user/newTweet', function (req,res) {//para postar um novo tweet é necessario que o usuario esteja logado e ele post em sua propria timeline
    if (req.session.authenticated && req.params.user == req.session.username) {
        var novoTweet = new tweet();
        novoTweet.postedById = req.body.postedById;
        novoTweet.postedBy = req.body.postedBy;
        novoTweet.message = req.body.message;
        novoTweet.save(function (err, tweetSalvo) {
            if (err) {
                console.log(err.message);
                res.status(500).send();
            }
            res.status(200).send();
        })
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.get('/:user',function (req, res) {//aqui mostrara todos os tweets do usuario e de quem ele segue
    function comp(a, b) {
        return new Date(a.time).getTime() - new Date(b.time).getTime();
    }
    if (req.session.authenticated) {
        usuario.findOne({"username":req.params.user}, function (err, result) {
            tweet.find({$or:[{"postedById": result.follow},{ "postedBy": req.params.user}]}, function (erro, resul) {
                if(erro) res.status(500).send();
                res.status(200).send(resul.sort(comp));
            })
        })
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.get('/:user/:postedBy', function (req,res) {//aqui o user entrara na timeline do usuario (postedby)
    if (req.session.authenticated) {
        tweet.find({"postedBy": req.params.postedBy}, function (err, result) {
            res.send(result);
        });
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.post('/:user/:postedBy/follow', function (req,res) {//na pagina de outro usuario, quando colocar o /follow, o user passa a seguir o posrtedby
    if (req.session.authenticated && req.params.user == req.session.username) {
        usuario.findOne({"username":req.params.postedBy},function (err,result) {
            if(req.params.user != req.params.postedBy) {
                usuario.findOneAndUpdate({"username": req.params.user}, {$addToSet: {follow: result._id}}, function (erro, resul) {
                    if (err) res.stat(500).send();
                    res.status(200).send();
                });
            }
            res.status(403).send();
        })
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.get('/:user/:postedBy/:tweet', function (req, res) {//para ver os comentarios de um tweet na pagina de outro usuario
    if (req.session.authenticated) {
        tweet.findOne({"_id": req.params.tweet}, function (err, result) {
            comentario.find({"_id": result.comments}, function (err, resultado) {
                res.status(200).send([result, resultado]);
            })
        });
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.get('/:user/:tweet', function (req, res) {//para ver os comentarios de um tweet na propria timeline do usuario
    if (req.session.authenticated) {
        tweet.findOne({"_id": req.params.tweet}, function (err, result) {
            comentario.find({"_id": result.comments}, function (err, resultado) {
                res.status(200).send([result, resultado]);
            })
        });
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.post('/:user/:postedBy/:tweet/newComment',function (req, res) {//para inserir um comentario no tweet de outro usuario
    if (req.session.authenticated && req.params.user == req.session.username) {
        var novoComentario = comentario();
        novoComentario.postedById = req.body.postedById;
        novoComentario.postedBy = req.body.postedBy;
        novoComentario.tweetId = req.body.tweetId;
        novoComentario.message = req.body.message;

        novoComentario.save(function (err, comentarioSalvo) {
            if (err) {
                console.log(err.message);
                res.status(500).send();
            }
            tweet.findByIdAndUpdate({_id: novoComentario.tweetId}, {$addToSet: {comments: novoComentario._id}}, function (err, ok) {
                if (err) res.status(500).send();
                else res.status(200).send();
            });
            res.status(200).send();
        })
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});
router.post('/:user/:tweet/newComment',function (req, res) {//para inserir um comentario no tweet do proprio usuario
    if (req.session.authenticated && req.params.user == req.session.username) {
        var novoComentario = comentario();
        novoComentario.postedById = req.body.postedById;
        novoComentario.postedBy = req.body.postedBy;
        novoComentario.tweetId = req.body.tweetId;
        novoComentario.message = req.body.message;

        novoComentario.save(function (err, comentarioSalvo) {
            if (err) {
                console.log(err.message);
                res.status(500).send();
            }
            tweet.findByIdAndUpdate({_id: novoComentario.tweetId}, {$addToSet: {comments: novoComentario._id}}, function (err, ok) {
                if (err) res.status(500).send();
                else res.status(200).send();
            });
            res.status(200).send();
        })
    }
    else {
        res.status(401).send({"message": "Necessario autenticação"});
    }
});


module.exports = router;
